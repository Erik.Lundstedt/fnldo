#!/bin/env bash

rm -f fennelFiles/*


styleFile="makeorg.fnl"
styleFile="makemd.fnl"


cp do.fnl       fennelFiles/main.fnl
cp util.fnl     fennelFiles/util.fnl
cp config.fnl   fennelFiles/config.fnl
cp includes.fnl fennelFiles/includes.fnl
cp $styleFile   fennelFiles/genlist.fnl
echo "#!/bin/env fennel"|cat - 	fennelFiles/includes.fnl fennelFiles/util.fnl fennelFiles/genlist.fnl fennelFiles/config.fnl fennelFiles/main.fnl >fennelFiles/fnldo
chmod a+rwx fennelFiles/fnldo
#	cp fennelFiles/fnldo /home/erik/bin/fnldo
