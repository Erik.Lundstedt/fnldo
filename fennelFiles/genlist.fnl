
(fn getlist [list nested]
(local retbl [])
(local level (. list "indentation"))

(if (and (= nested false) (= level 1))
	(table.insert retbl "# ")
)



(table.insert retbl (.. (. list "name") "\n"));;insert the name/title into the list that gets returned later



(each [k v (ipairs (. list "content"))];;for-each loop, loops thru the table at index "content" of the input-table
    (table.insert retbl (.. (indent level " ") "[ ] " v (iflen k (length (. list "content")))));;insert the items and some styling in the table we return later, allso make sure to format correctly depending on if it is a "sublist"(the indentation  level is more than 2)
    );;this ends the for(each) loop

(table.concat retbl)
)
